#!/bin/bash
set -ex
sudo apt-get install --yes shellcheck wget
go_archive=go1.13.3.linux-amd64.tar.gz
wget "https://dl.google.com/go/$go_archive"
tar -xf "$go_archive"
export GOROOT=/usr/local/go
sudo mv go "$GOROOT"
export PATH="$GOROOT/bin:$PATH"
pip3 install pre-commit pylint
pre-commit run --all-files
