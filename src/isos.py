"""Managing ISO images"""

import typing

import api.lazy
import api.spec
import commandline
import connections
import tables
import projects
import vms


def complete_iso(**kwargs):
    """complete ISO image name argument"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        return [
            iso[key] for iso in get_list(connection) for key in ["name", "id"]
        ]


Name = typing.NewType("Name", str)
Name.__doc__ = """\
    ISO image name
    """

commandline.set_completer(Name, complete_iso)

Bootable = typing.NewType("Bootable", bool)
Bootable.__doc__ = """\
    bootable ISO
"""

LIST_SPEC = ["id", "name", "size", "_zoneid"]

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)


@commandline.RegisterCategory
class Iso(commandline.Category):
    """managing ISO images"""

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[ListFilter],
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """list ISO images"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            rows = get_list(connection, **connection.cloudstack_args())
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def register(
        self,
        project: projects.Name,
        name: Name,
        url: commandline.URL,
        display_text: typing.Optional[commandline.DisplayText] = None,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
        bootable: Bootable = Bootable(False),
    ):
        """register an ISO image

        The command prints the newly created ID associated to the ISO image.
        The image will be available once fetched by Cloudstack: you will have
        to check the image availability with the "iso list" subcommand."""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            if display_text is None:
                displaytext: str = name
            else:
                displaytext = display_text
            response = connection.cloudstack.registerIso(
                displaytext=displaytext,
                name=name,
                url=url,
                zoneid=connection.zone_id,
                bootable=bootable,
            )
            print(response["iso"][0]["id"])

    @commandline.RegisterAction
    def attach(
        self,
        project: projects.Name,
        iso: Name,
        machine: vms.Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ):
        """attach an ISO to a virtual machine"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            iso_list = get_list(connection, **connection.cloudstack_args())
            iso_id = find(iso_list, iso)
            vm_list = vms.get_list_from_cloudstack(
                connection, **connection.cloudstack_args()
            )
            vm_id, _selected_vm = vms.find(
                tables.dict_of_list(vm_list, "id"),
                project,
                machine,
                required=True,
            )
            connection.cloudstack.attachIso(id=iso_id, virtualmachineid=vm_id)

    @commandline.RegisterAction
    def detach(
        self,
        project: projects.Name,
        machine: vms.Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ):
        """detach an ISO from a virtual machine"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            vm_list = vms.get_list_from_cloudstack(
                connection, **connection.cloudstack_args()
            )
            vm_id, _selected_vm = vms.find(
                tables.dict_of_list(vm_list, "id"),
                project,
                machine,
                required=True,
            )
            connection.cloudstack.detachIso(virtualmachineid=vm_id)

    @commandline.RegisterAction
    def delete(
        self,
        project: projects.Name,
        iso: Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ):
        """delete an ISO"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            iso_list = get_list(connection, **connection.cloudstack_args())
            iso_id = find(iso_list, iso)
            connection.cloudstack.deleteIso(id=iso_id)


def find(isos, name: str) -> str:
    """find an ISO"""
    return tables.find_offer(
        {iso["id"]: iso["name"] for iso in isos}, name, "ISO"
    )


def get_list(connection, **args):
    """enumerate ISO images"""
    isos = connection.cloudstack.listIsos(fetch_list=True, **args)
    return api.spec.remap_dict_list(isos, LIST_SPEC)
