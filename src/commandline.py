"""Command-line parsing"""

import argparse
import collections
import distutils.util
import inspect
import logging
import os
import re
import shutil
import sys
import textwrap
import typing

import cs.client
import inflection
import jenkins

import api.connection


_argcomplete_imported = False

try:
    import argcomplete

    _argcomplete_imported = True

    def try_argcomplete(parser: argparse.ArgumentParser) -> None:
        argcomplete.autocomplete(parser)

except ImportError:

    def try_argcomplete(parser: argparse.ArgumentParser) -> None:
        pass


# See https://github.com/python/typeshed/issues/3500#issuecomment-560958608
if sys.version_info >= (3, 8):
    # pylint: disable=invalid-name,no-member
    typing_extensions = typing
    get_origin = typing.get_origin
    get_args = typing.get_args
else:
    import typing_extensions

    def get_origin(type_):
        """Reimplementation of typing.get_origin for Python <3.8"""
        try:
            return type_.__origin__
        except AttributeError:
            return None

    def get_args(type_):
        """Reimplementation of typing.get_origin for Python <3.8"""
        try:
            return type_.__args__
        except AttributeError:
            return None


try:
    CREDENTIALS_FILENAME = os.environ["CI_CREDENTIALS"]
except KeyError:
    CREDENTIALS_FILENAME = os.path.join(
        os.environ["HOME"], ".ci_credentials.yml"
    )

DEFAULT_TIMEOUT = 30
DEFAULT_RETRY = 0


URL = typing.NewType("URL", str)
URL.__doc__ = """\
    URL to where the file should be fetched
    """


DisplayText = typing.NewType("DisplayText", str)
DisplayText.__doc__ = """\
    text used for display purpose [default: same as name]
    """


class Config(typing.NamedTuple):
    """configuration flags"""

    verbose: bool
    quiet: bool
    credentials: str
    timeout: int
    retry: int
    qualif: bool
    proxy: typing.Optional[str]
    compact: bool


class Failure(Exception):
    """fatal error with CI: the error message will be printed on standard error
    and the program exits"""


def handle() -> None:
    """entry-point of the command-line tool"""
    parser = argparse.ArgumentParser(
        description="Command-line for ci.inria.fr."
    )
    parser.add_argument(
        "--verbose", action="store_true", help="log network requests"
    )
    parser.add_argument(
        "--quiet", action="store_true", help="hide success messages"
    )
    parser.add_argument(
        "--credentials",
        default=CREDENTIALS_FILENAME,
        help="""
            credentials filename (default: CI_CREDENTIALS environment variable
            if set, ~/.ci_credentials.yml otherwise)
            """,
    )
    parser.add_argument(
        "--timeout",
        action="store",
        type=int,
        default=DEFAULT_TIMEOUT,
        help=f"time out for requests in second (default: {DEFAULT_TIMEOUT})",
    )
    parser.add_argument(
        "--retry",
        action="store",
        type=int,
        default=DEFAULT_RETRY,
        help=f"number of retry (default: {DEFAULT_RETRY})",
    )
    parser.add_argument(
        "--qlf-ci",
        dest="qlfci",
        action="store_true",
        help="use qualification platform qlf-ci.inria.fr",
    )
    parser.add_argument(
        "--compact",
        dest="compact",
        action="store_true",
        help="do not align columns in lists",
    )
    parser.add_argument("--proxy", help="proxy to use")
    category_subparsers = parser.add_subparsers(help="category selection")
    RegisterCategory.register_categories(category_subparsers)
    try_argcomplete(parser)
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    if not args.quiet:
        logging.basicConfig(level=logging.INFO)
    config: Config = Config(
        verbose=args.verbose,
        quiet=args.quiet,
        credentials=args.credentials,
        timeout=args.timeout,
        retry=args.retry,
        qualif=args.qlfci,
        proxy=args.proxy,
        compact=args.compact,
    )
    try:
        try:
            func = args.func
        except AttributeError:
            raise Failure(f"Usage: {parser.prog} --help")
        try:
            func(config, args)
        except cs.client.CloudStackApiException as error:
            raise Failure(error.error["errortext"])
    except (
        Failure,
        api.connection.QueryError,
        api.spec.Failure,
        jenkins.NotFoundException,
    ) as error:
        output_message(str(error), file=sys.stderr)
        sys.exit(1)
    except KeyboardInterrupt:
        # http://www.tldp.org/LDP/abs/html/exitcodes.html
        # 130 Script terminated by Control-C
        sys.exit(130)


class Category:
    """A sub-command at the first level of ci command-line"""

    def __init__(self):
        self.__config = None

    @property
    def config(self):
        """returns the current configuration options"""
        if self.__config is None:
            raise Failure("configuration is not available yet")
        return self.__config

    def add_actions(self, action_parsers):
        """add actions registered by @RegisterAction to parser"""
        try:
            actions = getattr(type(self), "_actions")
        except AttributeError:
            # No registered action
            return
        for action in actions:
            self.__add_action(action_parsers, action)

    def __add_action(self, action_parsers, action):
        name = inflection.dasherize(action.__name__)
        action_help = action.__doc__
        parser = add_parser(action_parsers, name, action_help)
        sig = inspect.signature(action)
        parameters = collections.OrderedDict(sig.parameters)
        parameters.popitem(last=False)  # self
        converters = [
            convert_parameter(parser, parameter)
            for parameter in parameters.values()
        ]

        # Proxy for self argument
        def do_action(config: Config, args):
            self.__config = config
            args = [converter(args) for converter in converters]
            action(self, *args)

        parser.set_defaults(func=do_action)


class RegisterCategory:
    """@RegisterCategory is a decorator for Category subclasses to register them
    for the command-line. A manual marking decorator is preferred rather than
    considering all subclasses, since it may be desirable to declare
    intermediate sub-classes."""

    __categories: typing.List[Category] = []

    def __init__(self, cls):
        type(self).__categories.append(cls)
        self.__cls = cls

    def __call__(self, cls):
        return cls

    @classmethod
    def register_categories(cls, category_subparsers):
        """register all marked @RegisterCategory-ies in argparser"""
        for cat in cls.__categories:
            cat_help = cat.__doc__
            # 'SSHKey' -underscore-> 'ssh_key' -dasherize-> 'ssh-key'
            name = inflection.dasherize(inflection.underscore(cat.__name__))
            parser = add_parser(category_subparsers, name, cat_help)
            action_parsers = parser.add_subparsers()
            instance = cat()
            instance.add_actions(action_parsers)


# pylint: disable=too-few-public-methods
class RegisterAction:
    """@RegisterAction is a decorator for methods in sub-classes of Category.

    To allow documentation to be an arbitrary expression evaluated as a string
    (for instance, f-strings), this decorator takes an optional parameter `doc'
    whose contents is substituted to the original __doc__ of the decorated
    function."""

    def __init__(self, *args, doc=""):
        try:
            (self.__decorated,) = args
        except ValueError:
            self.__decorated = None
            self.__doc = doc

    # __set_name__ was introduced in Python 3.6
    # pylint: disable=line-too-long
    # See this answer: https://stackoverflow.com/questions/3589311/get-defining-class-of-unbound-method-object-in-python-3/54316923#54316923
    def __set_name__(self, owner, name):
        try:
            actions = getattr(owner, "_actions")
        except AttributeError:
            actions = []
            setattr(owner, "_actions", actions)
        actions.append(self.__decorated)
        setattr(owner, name, self.__decorated)

    def __call__(self, *args, **_kwargs):
        # This __call__ can take any arguments to please mypy that confuses this
        # call with the decorated one.
        (decorated,) = args
        self.__decorated = decorated
        decorated.__doc__ = self.__doc
        return self


def convert_parameter(parser, parameter):
    """convert and register action parameter"""
    name = parameter.name
    annotation = parameter.annotation
    options = {}
    annotation, is_list, converter = convert_parameter_list(
        annotation, options, name
    )
    try:
        annotation = get_optional_basetype(annotation)
    except TypeError:
        pass
    if get_origin(annotation) is typing.Union:
        return convert_parameter_union(parser, parameter, annotation, is_list)
    if annotation.__doc__ is None:
        parameter_help = None
    else:
        help_cut = CutHelp(annotation.__doc__)
        parameter_help = help_cut.short
        append_epilog(parser, help_cut.epilog)
        options["help"] = parameter_help
    convenient_name = inflection.dasherize(name)
    if parameter.default == inspect.Parameter.empty:
        # no default value => positional argument
        option_name = name
    else:
        # default value => optional argument
        option_name = "--" + convenient_name
        options["default"] = parameter.default
    try:
        metavar = getattr(annotation, "metavar")
    except AttributeError:
        metavar = convenient_name.upper()
    options["metavar"] = metavar
    aliases = get_aliases(annotation)
    argtype = get_supertype(annotation)
    if argtype == bool:
        add_argument_bool(
            parser,
            option_name,
            name,
            aliases,
            parameter.default,
            help=parameter_help,
        )
    elif argtype == int:
        parser.add_argument(option_name, *aliases, type=int, **options)
    else:
        argument = parser.add_argument(option_name, *aliases, **options)
        register_argument_completer(argument, annotation)
    return converter


def convert_parameter_list(annotation, options, name):
    """handle list arguments"""
    try:
        annotation = get_iterable_basetype(annotation)
        is_list = True
    except TypeError:
        is_list = False
    if is_list:
        # List of arguments
        options["nargs"] = "*"

        def converter(args):
            arg = getattr(args, name)
            # No argument is represented as None: conversion to []
            if arg is None:
                return []
            return arg

    else:
        # Single argument
        def converter(args):
            return getattr(args, name)

    return annotation, is_list, converter


def add_argument_bool(
    parser: argparse.ArgumentParser,
    option_name: str,
    name: str,
    aliases: typing.Iterable[str],
    default: typing.Optional[bool],
    **options,
) -> None:
    """handle boolean arguments"""
    if (
        default == inspect.Parameter.empty
        or default is not None
        and not default
    ):
        parser.add_argument(
            option_name, *aliases, action="store_true", **options
        )
    elif default is None:
        group = parser.add_mutually_exclusive_group()
        group.add_argument(
            option_name, *aliases, action="store_const", const=True, **options
        )

        def add_argument_suffix(suffix, const):
            group.add_argument(
                option_name + suffix,
                *[alias + suffix for alias in aliases],
                dest=name,
                action="store_const",
                const=const,
                **options,
            )

        add_argument_suffix("=yes", True)
        add_argument_suffix("=no", False)


def convert_parameter_union(parser, parameter, annotation, is_list):
    """convert and register tagged union as parameter"""
    name = parameter.name
    branches = get_args(annotation)
    if is_list:
        group = parser
    else:
        group = parser.add_mutually_exclusive_group(
            required=parameter.default == inspect.Parameter.empty
        )
    has_constant = False
    constructors = {}
    for branch in branches:
        if branch is type(None):
            continue
        constructor = convert_parameter_union_branch(
            branch, name, group, is_list
        )
        if constructor is None:
            has_constant = True
        else:
            literal, tag_name, fields = constructor
            constructors[literal] = (tag_name, fields)

    def converter(args):
        if has_constant:
            constant = getattr(args, name)
            if constant is not None:
                return constant
        for literal, (tag_name, fields) in constructors.items():
            arguments = getattr(args, literal)
            if arguments is not None:
                if is_list:
                    result = [
                        {tag_name: literal, fields[0][0]: argument}
                        for argument in arguments
                    ]
                else:
                    result = {tag_name: literal}
                    for field, argument in zip(fields, arguments):
                        result[field[0]] = argument
                return result
        return parameter.default

    return converter


def convert_parameter_union_branch(branch, name, group, is_list):
    """convert a branch of a tagged union"""
    fields = list(branch.__annotations__.items())
    try:
        tag_name, tag_type = fields[0]
        literal = get_literal(tag_type)
    except (IndexError, TypeError):
        raise TypeError("only tagged unions are supported")
    options = {"help": branch.__doc__}
    nargs = len(fields) - 1
    if is_list:
        result = (literal, tag_name, fields[1:])
        options["nargs"] = "*"
    elif nargs == 0:
        result = None
        options["dest"] = name
        options["action"] = "store_const"
        options["const"] = {tag_name: literal}
    else:
        result = (literal, tag_name, fields[1:])
        options["nargs"] = nargs
    argument = group.add_argument("--" + literal, **options)
    if nargs == 1:
        register_argument_completer(argument, fields[1][1])
    return result


def get_optional_basetype(type_: type) -> type:
    """get_optional_basetype(type_) returns basetype if type_ is
    typing.Optional[basetype]"""
    if get_origin(type_) is typing.Union:
        args = get_args(type_)
        try:
            first, second = args
            if second is type(None):
                return first
        except ValueError:
            pass
    raise TypeError("type is not typing.Optional")


def get_iterable_basetype(type_: type) -> type:
    """get_iterable_basetype(type_) returns basetype if type_ is
    typing.Iterable[basetype] or typing.List[basetype]."""
    origin = get_origin(type_)
    # typing.get_origin(typing.List[T]) is list
    if origin is collections.abc.Iterable or origin is list:
        args = get_args(type_)
        try:
            (arg,) = args
            return arg
        except ValueError:
            pass
    raise TypeError("type is neither typing.Iterable not typing.List")


def get_literal(type_: type):
    """get_literal(type_) returns l if type_ is
    typing.Literal[l]."""
    if get_origin(type_) is typing_extensions.Literal:
        args = get_args(type_)
        try:
            (arg,) = args
            return arg
        except ValueError:
            pass
    raise TypeError("type is not typing.Literal")


def get_supertype(type_: type) -> type:
    """get_supertype(type_) extracts subtypes annotations from type_."""
    try:
        while True:
            type_ = getattr(type_, "__supertype__")
    except AttributeError:
        return type_


class CutHelp:
    """
    If an help string contain an empty line, the paragraph before the
    empty line is the short help, displayed just after the command name
    or the parameter name, and everything below the empty line is appended
    to the epilog, at the end of the help message.
    """

    def __init__(self, help_str: str):
        try:
            self.__short, epilog_str = re.split(
                "\n[ \t]*\n", help_str, maxsplit=1
            )
            self.__epilog: typing.Optional[str] = epilog_str
        except ValueError:
            self.__short = help_str
            self.__epilog = None

    @property
    def short(self) -> str:
        """the first paragraph of the message (before the first empty line)"""
        return self.__short

    @property
    def epilog(self) -> typing.Optional[str]:
        """the message after the first empty line"""
        return self.__epilog


def append_epilog(parser, addendum: typing.Optional[str]) -> None:
    """if addendum is not None, the contents is added to the parser epilog"""
    if addendum is not None:
        epilog = parser.epilog
        if epilog is None:
            parser.epilog = addendum
        else:
            parser.epilog = epilog + "\n" + addendum


def register_argument_completer(argument, annotation):
    """declare as argument completer the completer set in the annotation,
    if any"""
    try:
        argument.completer = getattr(annotation, "completer")
    except AttributeError:
        pass


def get_complete_argument_index(parsed_args):
    """return the index of the argument being completed"""
    if parsed_args.parameter is None:  # name
        return None
    return len(parsed_args.parameter)


def capitalize_preserve_case(text: str) -> str:
    """capitalize first letter while preserving the case of the other letters"""
    return text[0].upper() + text[1:]


def add_parser(parsers, name: str, help_str: typing.Optional[str]):
    """add a parser to a sub-parser choice, cutting the help string"""
    if help_str is not None:
        help_cut = CutHelp(help_str)
        help_short = help_cut.short
        epilog = help_cut.epilog
        description = capitalize_preserve_case(help_short) + "."
        parser_help: typing.Optional[str] = help_short
    else:
        parser_help = None
        description = ""
        epilog = None
    return parsers.add_parser(
        name, description=description, help=parser_help, epilog=epilog
    )


def set_completer(type_: type, completer) -> None:
    """register completer for type"""

    def completer_proxy(**kwargs):
        # This proxy makes argcomplete not to silent failures
        try:
            return completer(**kwargs)
        except Exception as exc:  # pylint: disable=broad-except
            if _argcomplete_imported:
                argcomplete.warn(exc)

    setattr(type_, "completer", completer_proxy)


def add_alias(type_: type, alias: str) -> None:
    """add an alias for options of the given type"""
    try:
        aliases = getattr(type_, "aliases")
    except AttributeError:
        aliases = []
        setattr(type_, "aliases", aliases)
    aliases.append(alias)


def get_aliases(type_: type) -> typing.Iterable[str]:
    """get an iterator for enumerating aliases associated to type"""
    try:
        # Return an iterator to only give read-only access
        return getattr(type_, "aliases").__iter__()
    except AttributeError:
        return []


def set_metavar(type_: type, metavar: str) -> None:
    """set the name of the metavariable used in the help message for options
    of the given type"""
    setattr(type_, "metavar", metavar)


# pylint: disable=line-too-long
# See https://stackoverflow.com/questions/715417/converting-from-a-string-to-boolean-in-python/18472142#18472142
def str_to_bool(value: str) -> bool:
    """convert a string to a Boolean with a nice error message"""
    try:
        # mypy does not appear to know about distutils.util
        return bool(distutils.util.strtobool(value))  # type: ignore
    except ValueError:
        raise Failure(
            f"""
            Expected bool (y/yes/t/true/on/1 or n/no/f/false/off/0) but got
            '{value}'
            """
        )


def str_to_int(value: str) -> int:
    """convert a string to an integer with a nice error message"""
    try:
        return int(value)
    except ValueError:
        raise Failure(f"Expected number but got '{value}'")


LazyInt = typing.Callable[[], int]


def str_to_relative(value: str) -> typing.Callable[[LazyInt], int]:
    """interpret a string which contains a number possibly preceded
    by +, -, * or /: if the string is a plain number, the resulting function
    ignores its argument (which is therefore not evaluated) and returns
    this number; if the string begins by an operator, the resulting function
    evaluates its argument and computes the operation."""
    if value == "":
        raise Failure("Size cannot be empty string")
    symbol = value[0]
    if symbol == "+":
        new_value = str_to_int(value[1:])
        return lambda old_value: old_value() + new_value
    if symbol == "-":
        new_value = str_to_int(value[1:])
        return lambda old_value: old_value() - new_value
    if symbol == "*":
        new_value = str_to_int(value[1:])
        return lambda old_value: old_value() * new_value
    if symbol == "/":
        new_value = str_to_int(value[1:])
        return lambda old_value: old_value() // new_value
    new_value = str_to_int(value)
    return lambda _old_value: new_value


def output_message(text: str, file=sys.stdout) -> None:
    """output text in file, wrapping the text by respecting the terminal
    width."""
    width = shutil.get_terminal_size().columns
    print(textwrap.fill(re.sub("[ \n]+", " ", text.strip()), width), file=file)


def expect_key(dictionary, key):
    """expect key to be in dictionary, and print an error message otherwise"""
    try:
        return dictionary[key]
    except KeyError:
        raise Failure(f"Missing field '{key}'")


def report_invalid(invalid: typing.List[str]) -> None:
    """raise an error if some requests have failed"""
    if invalid:
        raise Failure(f"Invalid server response for {', '.join(invalid)}.")


Filename = typing.NewType("Filename", str)
Filename.__doc__ = "filename"

OutputFilename = typing.NewType("OutputFilename", Filename)
OutputFilename.__doc__ = "output filename"
add_alias(OutputFilename, "-o")
set_metavar(OutputFilename, "FILENAME")
