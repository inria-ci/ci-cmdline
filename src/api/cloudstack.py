"""
CloudStack API

Enable password-based authentication
"""

import dataclasses
import typing

import cs.client
import requests

import api.connection


@dataclasses.dataclass(frozen=True)
class ReprepareRequestSession:
    """cs.client.CloudStack prepares every request with the prepare method of
    the request itself instead of preparing it the prepare_request method of
    the session, which prevents the request to use the session cookies.
    This class exposes the send method of requests.Session, the only method
    used by cs.client.CloudStack and prepares the request properly before
    sending."""

    session: requests.Session

    def send(self, prepped, **kwargs):
        """reprepare and send a request"""
        args = {}
        try:
            args["data"] = getattr(prepped, "body")
        except AttributeError:
            pass
        try:
            args["params"] = getattr(prepped, "params")
        except AttributeError:
            pass
        req = requests.Request(
            prepped.method, prepped.url, headers=prepped.headers, **args
        )
        reprepped = self.session.prepare_request(req)
        return self.session.send(reprepped, **kwargs)

    def __enter__(self):
        """session is used as a context manager in cs.client.CloudStack but
        we bypass that"""
        return self

    def __exit__(self, *args, **kwargs):
        pass

    def close(self):
        """close method is not used by cs.client.CloudStack but is used by
        class users like CI command-line"""
        self.session.close()


class CloudStackExt(cs.client.CloudStack):
    """CloudStack API with password-based authentication"""

    def __init__(
        self,
        endpoint: str,
        credentials: api.connection.Credentials,
        subdomain: typing.Optional[str] = None,
        **kwargs,
    ):
        actual_session = requests.Session()
        data = {
            "command": "login",
            "username": credentials.username,
            "password": credentials.password,
            "domain": "/ci" if subdomain is None else f"/ci/{subdomain}",
            "response": "json",
        }
        response = actual_session.post(endpoint, data)
        if response.status_code != 200:
            raise api.connection.QueryError("Unable to connect Cloudstack")
        response_json = response.json()
        self.__domainid = response_json["loginresponse"]["domainid"]
        wrapped_session = ReprepareRequestSession(actual_session)
        self.__actual_session = actual_session
        super().__init__(
            endpoint, "", "", session=wrapped_session, method="POST", **kwargs
        )

    @property
    def actual_session(self):
        """return underlying session"""
        return self.__actual_session

    @property
    def domainid(self):
        """return domain id obtained on login"""
        return self.__domainid
