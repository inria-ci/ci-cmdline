"""Dictionary and text specifications"""

import collections
import types
import typing


TextSpec = typing.Mapping[str, typing.Callable[[str], bool]]


class Failure(Exception):
    """raised when a specification is not fulfilled"""


def get_spec_text(kind: str, spec: TextSpec) -> str:
    """return a readable version of a text specification"""
    return kind + " " + ", and ".join(spec.keys()) + "."


def check_text_spec(text: str, kind: str, spec: TextSpec) -> None:
    """check that a text follows a text specification"""
    violated_rules = [
        rule for rule, predicate in spec.items() if not predicate(text)
    ]
    if violated_rules != []:
        raise Failure(
            f"""\
            {text} is not a valid {kind}:
            {kind} {', and '.join(violated_rules)}.
            """
        )


AnyInterpreter = typing.Callable[[typing.Any], typing.Any]

DictColumnSpec = typing.Union[str, typing.Tuple[str, AnyInterpreter]]
"""Column specification: identity is the default interpreter."""

DictSpec = typing.Union[
    typing.Iterable[str], typing.Mapping[str, DictColumnSpec]
]
"""A table specification maps key to column name and an interpreter for cells.
If it is a simple list, the remapping is just a projection to a subset of keys.
If a key has a leading '_', the value is taken from the key with the leading '_'
removed."""


def identity(arg):
    """identity function"""
    return arg


def remap_dict(
    dictionary: typing.Mapping[str, typing.Any],
    spec: DictSpec,
    default_interpreter: AnyInterpreter = identity,
) -> typing.Mapping[str, typing.Any]:
    """Change dictionary entries according to a table specification"""
    if not isinstance(spec, collections.abc.Mapping):
        spec = {
            column: column[1:] if column[0] == "_" else column
            for column in spec
        }

    def get_column(column: DictColumnSpec):
        if isinstance(column, str):
            key = column
            interpreter = default_interpreter
        else:
            key, interpreter = column
        return interpreter(dictionary.get(key, None))

    return {key: get_column(column) for key, column in spec.items()}


def remap_dict_list(
    items: typing.Iterable[typing.Mapping[str, typing.Any]],
    spec: DictSpec,
    default_interpreter: AnyInterpreter = identity,
) -> typing.List[typing.Mapping[str, typing.Any]]:
    """Change list of dictionaries according to a table specification"""
    return [
        remap_dict(dictionary, spec, default_interpreter=default_interpreter)
        for dictionary in items
    ]


class Listable:
    """listable element"""

    def __init__(self, as_map=None):
        self.__as_map = as_map

    def __repr__(self):
        return str(self.__as_map)

    @property
    def as_map(self):
        """return the dictionary representation of the element"""
        return types.MappingProxyType(self.__as_map)
