"""Managing deleted projects"""

import typing

import api.crawler
import api.portal
import commandline
import connections
import tables

DeletedProjectListFilter = typing.NewType("DeletedProjectListFilter", str)
tables.init_list_filter_type(
    DeletedProjectListFilter, api.crawler.DELETED_PROJECT_LIST_SPEC
)


def complete_deleted_project(**kwargs):
    """complete project arguments"""
    with connections.NoninteractiveConnections(
        kwargs["parsed_args"].credentials
    ) as connection:
        return match_table(connection.portal.projects.deleted).keys()


Name = typing.NewType("Name", str)
Name.__doc__ = "project name"
commandline.set_completer(Name, complete_deleted_project)


@commandline.RegisterCategory
class DeletedProject(commandline.Category):
    """managing deleted projects"""

    @commandline.RegisterAction
    def list(
        self, list_filter: typing.Iterable[DeletedProjectListFilter]
    ) -> None:
        """list deleted projects"""
        with connections.Connections(self.config) as connection:
            tables.filter_and_show_listable(
                connection.portal.projects.deleted,
                list_filter,
                self.config.compact,
            )

    @commandline.RegisterAction
    def recover(self, project: Name) -> None:
        """recover a project"""
        with connections.Connections(self.config) as connection:
            for project_api in tables.match_dict(
                match_table(connection.portal.projects.deleted), project
            ):
                project_api.recover()

    @commandline.RegisterAction
    def really_delete(self, project: Name) -> None:
        """really delete a project"""
        with connections.Connections(self.config) as connection:
            for project_api in tables.match_dict(
                match_table(connection.portal.projects.deleted), project
            ):
                project_api.really_delete()


def match_table(deleted_list: typing.Iterable[api.portal.DeletedProject]):
    """convert list of members into a dictionary indexed by name and by
    e-mail."""
    return {
        deleted_project.short_name: deleted_project
        for deleted_project in deleted_list
    }
